# VR Chatbot API

API utilizada para fazer a integração com o chatbot do Facebook.

## v1
- 1.5.0 - Ajustes para implementação de Basic Auth

## Docker

- Para fazer o build 
  {
    docker build -t 18.231.50.123:5000/chatbot .
  }

- Para executar
  { 
    docker run --name chatbot -p 5000:5000 \
    -e CHAVE_EMITTER=EMITTER \
    -e CHAVE_TOKEN=TOKEN \
    -e ORDER_URL=http://pedidos.vr.com.br \
    -e SERVER_URL=https://servicos.vr.com.br/mobileservicevr \
    -d 18.231.50.123:5000/chatbot
  }

## Endpoints

### Saldo

- **[```GET``` api/saldo]**

#### Descrição
Retorna o saldo do cartão.

#### Parâmetros
- **cartao** _(obrigatório)_ — O número do cartão que deseja consultar o saldo. O número do cartão é composto por 16 caracteres sem espaço e sem ponto.

#### Example
**Request**

    https://chatbotapi.vr.com.br/api/saldo?cartao=6370360004528193

**Return** 

	{
	    "success": true,
	    "nome": "CARLOS A DOS SANTOS",
	    "cartao": "VR ALIM",
	    "saldo": 0.86
	}


#### Cartões

- **[```GET``` api/cartoes]**

#### Descrição
Retorna os cartões do beneficiário.

#### Parâmetros
- **cpf** _(obrigatório)_ — O CPF do beneficiário sem pontos e espaços.

#### Example
**Request**

    https://chatbotapi.vr.com.br/api/cartoes?cpf=25562907810

**Return** 

	{
	    "success": true,
	    "cartoes": [
	        {
	            "cartao": "VR REF",
	            "nome": "LEONARDO SELLANI",
	            "saldo": 4570.66
	        },
	        {
	            "cartao": "VR ALIM",
	            "nome": "LEONARDO SELLANI",
	            "saldo": 558.71
	        },
	        {
	            "cartao": "VR NATAL",
	            "nome": "LEONARDO SELLANI",
	            "saldo": 0
	        },
	        {
	            "cartao": "VR AUTO",
	            "nome": "LEONARDO SELLANI",
	            "saldo": 517.25
	        }
	    ]
	}


#### Extrato

- **[```GET``` api/extrato]**

#### Descrição
Retorna o extrato de consumo do cartão informado.

#### Parâmetros
- **cartao** _(obrigatório)_ — O número do cartão que deseja consultar o extrato. O número do cartão é composto por 16 caracteres sem espaço e sem ponto.
- **cpf** _(obrigatório)_ — O CPF do beneficiário sem pontos e espaços.

#### Example
**Request**

    https://chatbotapi.vr.com.br/api/extrato?cartao=6274160006489858&cpf=25562907810

**Return** 

	

#### Pedido

- **[```GET``` api/pedido]**

#### Descrição
Retorna a situação do pedido.

#### Parâmetros
- **cnpj** _(obrigatório)_ — O CNPJ do RH solicitante.
- **protocolo** _(obrigatório)_ — O número de protocolo do pedido.

#### Example
**Request**

    https://chatbotapi.vr.com.br/api/pedidos?cnpj=20198280000100&protocolo=20180123001560

**Return** 

	{ 
		"success": true,
		"status": "Finalizado"
	}


#### Agendamento

- **[```GET``` api/agendamento]**

#### Descrição
Retorna as datas do agendamentos da próximas cargas nos cartões do beneficiário.

#### Parâmetros
- **cpf** _(obrigatório)_ — O CPF do beneficiário sem pontos e espaços.

#### Example
**Request**

    https://chatbotapi.vr.com.br/api/agendamento?cpf=25562907810

**Return** 

	{
	    "success": true,
	    "cartoes": [
	        {
	            "cartao": "VR REF",
	            "agendamento": "A confirmar"
	        },
	        {
	            "cartao": "VR ALIM",
	            "agendamento": "A confirmar"
	        },
	        {
	            "cartao": "VR NATAL",
	            "agendamento": "A confirmar"
	        },
	        {
	            "cartao": "VR AUTO",
	            "agendamento": "A confirmar"
	        }
	    ]
	}

