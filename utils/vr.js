var config = require('config'),
    sha1 = require('sha1'),
    IMEI = 'CHATBOT-API',
    package = require('../package.json');

var CHAVE_TOKEN = (process.env.CHAVE_TOKEN) ? 
  process.env.CHAVE_TOKEN :
  config.get('chaveToken');

var CHAVE_EMITTER = (process.env.CHAVE_EMITTER) ? 
  process.env.CHAVE_EMITTER :
  config.get('chaveEmitter');

module.exports = {

  getToken: function (cardNumber, cpfBeneficiario, dataInicial, dataFinal) {
    var temp = CHAVE_TOKEN + 'VRPAT' + cpfBeneficiario + cardNumber + 
    dataInicial + dataFinal + package.version + IMEI + this.getDate();
    temp = sha1(temp);
    console.log('token: ' + temp);
    return temp;
  },

  getDate: function (minusDays) {
    var currentDate = new Date()
    if (minusDays != null) currentDate.setDate(currentDate.getDate() - minusDays);
    
      var day = currentDate.getDate();
      if (day<10) {
        day = '0' + day;
      } 

    var month = currentDate.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    } 

    var year = currentDate.getFullYear()
    var value = day + "/" + month + "/" + year
    return value;
  },

  getEmitter: function () {
    var temp = CHAVE_EMITTER + this.getDate();
    temp = sha1(temp);
    return temp;
  }

};