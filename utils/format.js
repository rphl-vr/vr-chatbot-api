module.exports = {

	formatNome: function (str) {
	    return str.replace(/\w\S*/g, function(txt) {
	        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	    });
	},

	formatMoney: function (n) {
		if (n == undefined) return "-";
		var value = n.toLocaleString('pt-BR', {
			maximumFractionDigits: 2, 
			minimumFractionDigits: 2
		});
		return 'R$ ' + value;
	},

	formatDescricao: function (d) {
		if (d == undefined) return "-";
		if (d.startsWith('Consumo -')) {
			d = d.replace('Consumo - ', '').trim();
		}
		return d;
	},

	formatAplicacao: function (codigoAplicacao, nomeAplicacao) {
		switch (codigoAplicacao) {
			case 27: return 'VR Alimentação';
			case 28: return 'VR Auto';
			case 30: return 'VR Cultura';
			case 31: return 'VR Refeição';
			case 201: return 'VR Alimentação Cesta';
			case 202: return 'VR Alimentação Natal';
			case 204: return 'VR Auxílio Alimentação';
			case 205: return 'VR Saúde';
			default: return nomeAplicacao;
		}
	}

};