var express = require('express'),
	basicAuth = require('express-basic-auth')
	port = process.env.PORT || 5000,
	bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
  	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  	next();
});

// acesso aberto sem autenticação
app.use('/api', require('./routes'));

app.use(basicAuth({
    users: { 'hiplatform': 'Gwk-hUF-zwd-3zy' }
}));

// acesso com basic auth
app.use('/api/v1', require('./routes'));

app.listen(port, function() {
	console.log('server is ready on port: ' + port);
});