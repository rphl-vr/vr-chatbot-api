var express = require('express'),
	router = express.Router(),
	request = require('request'),
	package = require('../package.json'),
  	IMEI = 'CHATBOT-API',
  	config = require('config'),
  	format = require('../utils/format'),
  	vr = require('../utils/vr');

var SERVER_URL = (process.env.SERVER_URL) ? 
	process.env.SERVER_URL :
 	config.get('serverUrl');

router.get('/saldo', function(req, res) {
	console.log('/saldo');

	var cardNumber = req.query.cartao;
	console.log("número do cartão: " + cardNumber);
	
	if (!cardNumber) {
		res.status(400);
		res.send({success: false, msg: 'Não foi possível identificar o número do cartão.'});
		return;
	}

	if (cardNumber.length < 16) {
		res.status(400);
		res.send({success: false, msg: 'Número de cartão inválido.'});
		return;
	}

	var url = SERVER_URL + '/cartoes/consultarSaldoConta?' + 
	'cpfBeneficiario=&numeroTelefone=&imei=' + IMEI + '&versaoApp=' + package.version + '&chaveEmissor=' + vr.getEmitter() + 
	'&siglaEmissor=VRPAT&numeroCartao=' + cardNumber + '&';
	console.log('url: ', url);

	var options = {
		url: url,
		headers: {
			'User-Agent': 'request',
		  	'token': vr.getToken(cardNumber, '', '', '')
		}
	};

	request(options, function (error, response, body) {

	    if (!error && response.statusCode == 200) {
	    	
	    	var info = JSON.parse(body);
	      	console.log(info);

	      	var text;
	      	if (info.codigoResposta == 0) {

	      		var nomePersoCartao = format.formatNome(info.nomePersoCartao);
	      		var saldo = format.formatMoney(info.saldo);
	      		var nomeAplicacao = format.formatAplicacao(info.codigoAplicacao, info.nomeAplicacao);

	      		res.json({success: true, nome: nomePersoCartao, cartao: nomeAplicacao, saldo: saldo});

	    	} else {

	        	var codigoResposta = info.codigoResposta;
	        	switch (codigoResposta) {
	        		case -90:
	          		text = "Falha na comunicação com a API VR.";
	          		break;

	          		case -99:
	          		text = "Verifique se os dados fornecidos estão corretos e tente novamente."
	          		break;

	          		default:
	          		text = info.descricaoResposta;
	          		break;
	        	}

	        	res.status(500);
				res.send({success: false, msg: text});
	      	}
	    }
	});
});

router.get('/cartoes', function(req, res) {
	console.log('/cartoes');

	var cpf = req.query.cpf;
	console.log('cpf: ' + cpf);

	if (!cpf) {
		res.status(400);
		res.send({success: false, msg: 'Não foi possível identificar o CPF.'});
		return;
	}

	if (cpf.length < 11) {
		res.status(400);
		res.send({success: false, msg: 'CPF inválido.'});
		return;
	}

	var url = SERVER_URL + '/cartoes/consultarDetalhesCartoesBeneficiario?' + 
	'cpfBeneficiario=' + cpf + '&numeroTelefone=&imei=' + IMEI + '&isSva=true&versaoApp=' + package.version + 
	'&chaveEmissor=' + vr.getEmitter() + '&siglaEmissor=VRPAT&';
	console.log('url: ', url);

	var options = {
		url: url,
		headers: {
			'User-Agent': 'request',
		  	'token': vr.getToken('', cpf, '', '')
		}
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
	  		var info = JSON.parse(body);
	      	console.log(info);

	      	var text;
	      	if (info.codigoResposta == 0) {
	      		var cartoes = [];
		      	for (var i = 0; i < info.cartoes.length; i++) {
		      		var cartao = info.cartoes[i];

		      		var nomePersoCartao = format.formatNome(cartao.nomePersoCartao);
		      		var saldo = format.formatMoney(cartao.saldo);
		      		var nomeAplicacao = format.formatAplicacao(cartao.codigoAplicacao, cartao.nomeAplicacao);

		      		cartoes.push({
		      			cartao: nomeAplicacao,
		      			numero: cartao.numeroCartao, 
		      			nome: nomePersoCartao, 
		      			saldo: saldo
		      		});
		      	}

		      	res.json({success: true, nome: nomePersoCartao, cpf: cpf, cartoes: cartoes});

	      	} else {

		        var codigoResposta = info.codigoResposta;
		        switch (codigoResposta) {
		        	case -90:
					text = "Falha na comunicação com a API VR.";
					break;

					case -99:
					text = "Verifique se os dados fornecidos estão corretos e tente novamente."
					break;

					default:
					text = info.descricaoResposta;
					break;
	        	}

	        	res.status(500);
				res.send({success: false, msg: text});
	      	}
	    }
	});
});

router.get('/extrato', function(req, res) {
	console.log('extrato');

	var cpf = req.query.cpf;
	console.log('cpf: ' + cpf);

	if (!cpf) {
		res.status(400);
		res.send({success: false, msg: 'Não foi possível identificar o CPF.'});
		return;
	}

	if (cpf.length < 11) {
		res.status(400);
		res.send({success: false, msg: 'CPF inválido.'});
		return;
	}

	var numeroCartao = req.query.cartao;
	
	if (!numeroCartao) {
		res.status(400);
		res.send({success: false, msg: 'Não foi possível identificar o número do cartão.'});
		return;
	}

	if (numeroCartao.length < 16) {
		res.status(400);
		res.send({success: false, msg: 'Número de cartão inválido.'});
		return;
	}

	var dataInicial = vr.getDate(7);
	var dataFinal = vr.getDate();

	var url = SERVER_URL + '/cartoes/consultarExtratoConta?' + 
	'cpfBeneficiario=' + cpf + '&numeroTelefone=&imei=' + IMEI + '&isSva=true&versaoApp=' + 
	package.version + '&chaveEmissor=' + vr.getEmitter() + '&siglaEmissor=VRPAT&numeroCartao=' + numeroCartao +
	'&dataInicial=' + dataInicial + '&dataFinal=' + dataFinal;
	console.log('url: ', url);

	var options = {
		url: url,
		headers: {
			'User-Agent': 'request',
		  	'token': vr.getToken(numeroCartao, cpf, dataInicial, dataFinal)
		}
	};

	request(options, function (error, response, body) {
		console.log(response.statusCode);
	  	if (!error && response.statusCode == 200) {
	  		var info = JSON.parse(body);
	      	console.log(info);

	      	var text;
	      	if (info.codigoResposta == 0) {
	      		
				var transacoes = [];
				if (info.transacoes) {
			      	for (var i = 0; i < info.transacoes.length; i++) {
			      		var transacao = info.transacoes[i];

			      		var nomeAplicacao = format.formatAplicacao(info.codigoAplicacao, info.nomeAplicacao);
			      		var nomePersoCartao = format.formatNome(info.nomePersoCartao);
			      		var valorTransacao = format.formatMoney(transacao.valorTransacao);
			      		var descricaoTransacao = format.formatDescricao(transacao.descricaoTransacao);
			      		
			      		transacoes.push({
			      			dataHoraTransacao: transacao.dataHoraTransacao, 
			      			valorTransacao: valorTransacao, 
			      			tipoTransacao: transacao.tipoTransacao,
			      			descricaoTransacao: descricaoTransacao
			      		});
			      	}
			    }

		      	res.json({success: true, cartao: nomeAplicacao, nome: nomePersoCartao, transacoes: transacoes});

	      	} else {

		        var codigoResposta = info.codigoResposta;
		        switch (codigoResposta) {
		        	case -90:
					text = "Falha na comunicação com a API VR.";
					break;

					case -99:
					text = "Verifique se os dados fornecidos estão corretos e tente novamente."
					break;

					default:
					text = info.descricaoResposta;
					break;
	        	}

	        	res.status(500);
				res.send({success: false, msg: text});
	      	}
	    }
	});
});

router.get('/agendamento', function(req, res) {
	console.log('agendamento');

	var cpf = req.query.cpf;
	console.log('cpf: ' + cpf);

	if (!cpf) {
		res.status(400);
		res.send({success: false, msg: 'Não foi possível identificar o CPF.'});
		return;
	}

	if (cpf.length < 11) {
		res.status(400);
		res.send({success: false, msg: 'CPF inválido.'});
		return;
	}

	var emitter = vr.getEmitter();
	console.log("emitter: " + emitter);

	var url = SERVER_URL + '/cartoes/consultarDetalhesCartoesBeneficiario?' + 
	'cpfBeneficiario=' + cpf + '&numeroTelefone=&imei=' + IMEI + '&isSva=true&versaoApp=' + package.version + 
	'&chaveEmissor=' +  emitter + '&siglaEmissor=VRPAT&';
	console.log('url: ', url);

	var options = {
		url: url,
		headers: {
			'User-Agent': 'request',
		  	'token': vr.getToken('', cpf, '', '')
		}
	};

	request(options, function (error, response, body) {

	    if (!error && response.statusCode == 200) {

	      	var info = JSON.parse(body);
	      	console.log(info);

	      	var text;
	      
	      	if (info.codigoResposta == 0) {

      			var cartoes = [];
	      		for (var i = 0; i < info.cartoes.length; i++) {
	      			
	      			var cartao = info.cartoes[i];

	      			var nomeAplicacao = format.formatAplicacao(cartao.codigoAplicacao, cartao.nomeAplicacao);

					var agendamento = '';
					if (cartao.dataProximoCredito) {
			      		agendamento = cartao.dataProximoCredito;
			      	} else {
			      		agendamento = "A confirmar";
			      	}

	      			cartoes.push({cartao: nomeAplicacao, agendamento: agendamento});
	      		}

	      		res.json({success: true, cartoes: cartoes});

      		} else {

		        var codigoResposta = info.codigoResposta;
		        switch (codigoResposta) {
		        	case -90:
					text = "Falha na comunicação com a API VR.";
					break;

					case -99:
					text = "Verifique se os dados fornecidos estão corretos e tente novamente."
					break;

					default:
					text = info.descricaoResposta;
					break;
	        	}

	        	res.status(500);
				res.send({success: false, msg: text});
	      	}
	    }
	});
});

module.exports = router;