var express = require('express'),
	router = express.Router();

router.use('/', require('./cartoes'));
router.use('/', require('./pedidos'));

module.exports = router;