var express = require('express'),
	router = express.Router(),
	request = require('request'),
	soap = require('soap'),
  	config = require('config');

 var ORDER_URL = (process.env.ORDER_URL) ?
 	process.env.ORDER_URL :
 	config.get('orderUrl');

router.get('/pedidos', function(req, res) {

	var cnpj = req.query.cnpj;
	console.log('cnpj: ' + cnpj);
	if (!cnpj) {
		res.status(400).json({success: false, msg: 'Não foi possível identificar o CNPJ.'});
		return;
	}

	if (cnpj.length != 14) {
		res.status(400).json({success: false, msg: 'CNPJ inválido.'});
		return;
	}

	var protocolo = req.query.protocolo;
	console.log('protocolo: ' + protocolo);
	if (!protocolo) {
		res.status(400).json({success: false, msg: 'Não foi possível identificar o protocolo.'});
		return;
	}

	if (protocolo.length != 14) {
		res.status(400).json({success: false, msg: 'Número de protocolo inválido.'});
		return;
	}

	var url = ORDER_URL + '/PED-Pedido/api-integracao/pedidoOnline?wsdl';
	console.log('url: ' + url);
	var args = { requisicao: { tenantId: { cnpjRh: cnpj, siglaEmissor: 'VRPAT' }, protocolo: protocolo }};
  	soap.createClient(url, function(err, client) {
  		client.buscarDetalhePedido(args, function(err, result) {
  			if (!err) {

  				console.log(result);
  				console.log(result.resposta);
  				
  				var code = result.resposta.codigoRetorno.codigo;
  				console.log('code: ' + code);
  				if (code == 0) {

  					var status = result.resposta.detalhePedido.status;
  					console.log('status: ' + status);
  					res.json({success: true, msg: getStatus(status)});

  				} else {

  					var message = result.resposta.codigoRetorno.descricao;
  					res.status(500).json({ success: false, msg:message });

  				}

  			} else {
  				console.log(err);
  				res.status(500).json({ success: false, msg:'Aconteceu um erro recuperando o status do pedido.' });
  			}
      	});
	});

});

function getStatus(code) {
	switch (code) {
		case '0': return 'Criado'; break
		case '1': return 'Em digitação'; break
		case '2': return 'Processando arquivo'; break
		case '3': return 'Arquivo inválido'; break
		case '4': return 'Confirmado/Validado'; break
		case '5': return 'Aguardando liberação'; break
		case '6': return 'Liberado'; break
		case '7': return 'Em processamento'; break
		case '8': return 'Finalizado'; break
		case '9': return 'Cancelado'; break
		case '10': return 'Arquivo em tratamento'; break
		default: return 'Status não definido.'; break
	}
}

module.exports = router;